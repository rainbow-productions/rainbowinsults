const demon = document.getElementById('demon');
const nameInput = document.getElementById('name');
const insult = document.getElementById('insult');
const form = document.getElementById('insultForm');
const ver = document.getElementById('version')

function setVersion(){
    ver.innerHTML = 'Ver. 1.7.2'
}
// Set initial demon and insult messages
function setInitialMessages() {
    demon.innerHTML = '<img src="images/rainbowdemon.png"></img>';
    insult.innerHTML = 'Enter a name.';
    setVersion()
}

// Reset demon and insult messages after a delay
function resetMessages() {
    setTimeout(function () {
        setInitialMessages();
        nameInput.value = '';
    }, 3000);
}

// Function to execute when the name input changes
form.addEventListener('submit', function (event) {
    event.preventDefault();
    const nameValue = nameInput.value.trim().toLowerCase();

    // Check if the name is empty
    if (nameValue === '') {
        insult.innerHTML = 'Enter a name.';
        resetMessages();
        return;
    }

    if (nameValue.includes('layla', 'Layla')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = 'MARIOS PLAYROOM';
    } else if (nameValue.includes('donovan', 'Donovan')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = 'Bro was the asteroid that killed the dinosaurs.';
    } else if (nameValue.includes('jacoby', 'Jacoby')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's obsessed with Thomas the Train Engine";
    } else if (nameValue.includes('junior', 'Junior') || nameValue.includes('stephen')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = 'Bros brain is smooth like a slice of cheese!';
    } else if (nameValue.includes('river')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "I can't think of an insult for you because you are an insult to me.";
    } else if (nameValue.includes('nate') || nameValue.includes('nathan')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's obsessed with 'the duty'🍆🍆";
    } else if (nameValue.includes('adeline')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "The chair be like 'get up, walk around and get in trouble'.";
    } else if (nameValue.includes('damian')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro keeps millions of dollars in pokemon cards and waits for the value to crash.";
    } else if (nameValue.includes('eli') || nameValue.includes('elijah l.')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's paranoid ass can't even trust someone with a pack of cards.";
    } else if (nameValue.includes('ms. verwest') || nameValue.includes('verwest')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's the Wicked Witch of the West🧙‍♀️🧙‍♀️🧙‍♀️";
    } else if (nameValue.includes('bailey', 'Bailey')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's always making faces.";
    } else if (nameValue.includes('jace', 'Jace')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's really obsessed with touching Brian.";
    } else if (nameValue.includes('fuck this website')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Try entering your name and find out how bad we are!";
    } else if (nameValue.includes('/e free-admin')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "That's not going to work, bozo.";
    } else if (nameValue.includes('casey', 'Casey')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "I can't think of an insult for you because you're already one.";
    } else if (nameValue.includes('james', 'James')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro's boutta get a gun licence";
    } else if (nameValue.includes('aiden', 'Aiden')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "Bro has Skibidi Toilet Syndrome";
    } else if (nameValue.includes('teachers', 'Teachers')) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "You always have to be in our buisness.";
    } else if (nameValue.includes("https://www.youtube.com/watch?v=dQw4w9WgXcQ")) {
        demon.innerHTML = '<img src="images/rainbowdemon-laugh.gif"></img>';
        insult.innerHTML = "LOL! That won't work on me. You can't rickroll me LOL";
    } else {
        insult.innerHTML = 'Enter a name.';
    }

    resetMessages();
});

// Set initial messages when the page loads
setInitialMessages();
